let valor = 0;
let cantidadAcertados = 0;
let valorAnterior = 0;
let lblMensaje = document.getElementById('lblMensaje');
let lblAnteriores = document.getElementById('lblAnteriores');
let divJuego = document.getElementById('divJuego');
let divBotones= document.getElementById('divBotones');
let lblFinal= document.getElementById('lblFinal');

function Empezar()
{
    valor = 0;
    lblMensaje.innerHTML = "El numero Actual es : " + valor;    
    lblFinal.innerHTML="";
    lblAnteriores.innerHTML = "";
    divJuego.style.backgroundColor = 'white';
    divBotones.style.display = '';
}
function Mayor(){
    Calcular();
    Controlar('MAYOR');
}
function Menor(){
    Calcular();
    Controlar('MENOR');
}

function Calcular(){
    lblAnteriores.innerHTML =   valor +"<br>"+ lblAnteriores.innerHTML;
    valorAnterior = valor;
    valor = Math.floor(Math.random() * 10) + 1;   
    lblMensaje.innerHTML = "El numero Actual es : " + valor;    
}

function Controlar(operacion)
{
    let error = false;
    if (operacion == 'MAYOR')
    {
        if (valor < valorAnterior)
            error = true;
    }
    else
    {
        if (valor > valorAnterior)
            error = true;
    }
    if (error)
    {
        divJuego.style.backgroundColor = 'red';
        divBotones.style.display = 'none';
        lblFinal.innerHTML="Usted ha acertado " + cantidadAcertados;
    }
    else
    {
        cantidadAcertados++;
    }
    
}


